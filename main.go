package main

import (
	"gitlab.com/GoGerman/go-parser/cmd/app"
)

func main() {
	app.Run()
}
