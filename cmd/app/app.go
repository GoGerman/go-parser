package app

import (
	"gitlab.com/GoGerman/go-parser/internal/config"
	"gitlab.com/GoGerman/go-parser/internal/database"
	"gitlab.com/GoGerman/go-parser/internal/modules/handlers"
	"gitlab.com/GoGerman/go-parser/internal/modules/repository"
	"gitlab.com/GoGerman/go-parser/internal/modules/service"
	"gitlab.com/GoGerman/go-parser/internal/router"
	"log"
)

func Run() {
	conf := config.DB{
		Host:     "postgres",
		Port:     "5432",
		Password: "1234",
		User:     "user",
		Name:     "postgres",
		Driver:   "postgres",
	}
	//conf2 := config.DB{
	//	Host:     "mongo",
	//	Port:     "27017",
	//	Password: "1234",
	//	User:     "user",
	//	Name:     "mongo",
	//	Driver:   "mongo",
	//}
	postg, mongo, err := database.NewDB(conf)
	if err != nil {
		log.Fatalln(err)
	}
	repo := repository.NewVacancyRepository(postg, mongo)
	serv := service.NewVacancyService(repo)
	cont := handlers.NewVacancyController(serv)
	router.RunServer(cont)
}
