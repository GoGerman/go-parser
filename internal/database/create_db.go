package database

import (
	"context"
	"errors"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"gitlab.com/GoGerman/go-parser/internal/config"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const Create = `CREATE TABLE IF NOT EXISTS vacancies (
    id 						serial primary key,
    type                               varchar,
    date_posted                        varchar,
    title                              varchar,
    description                        varchar,
    identifier_type                    varchar,
    identifier_name                    varchar,
    identifier_value                   varchar,
    valid_through                      varchar,
    hiring_organization_type           varchar,
    hiring_organization_name           varchar,
    hiring_organization_logo           varchar,
    hiring_organization_same_as        varchar
                                     )`

func NewDB(dbConf config.DB) (*sqlx.DB, *mongo.Collection, error) {
	var dbs string

	switch dbConf.Driver {
	case "postgres":
		dbs = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
			dbConf.Host, dbConf.Port, dbConf.User, dbConf.Password, dbConf.Name)
		db, err := sqlx.Open("postgres", dbs)
		if err != nil {
			log.Fatalln(err)
		}
		err = db.Ping()
		if err != nil {
			log.Fatalln(err)
		}
		_, err = db.Exec(Create)
		if err != nil {
			log.Fatalln(err)
		}
		return db, nil, nil
	case "mongo":
		dbs = fmt.Sprintf("mongodb://%s:%s@%s:%s/%s",
			dbConf.User, dbConf.Password, dbConf.Host, dbConf.Port, dbConf.Name)
		client := options.Client().ApplyURI(dbs).SetAuth(options.Credential{
			Username: dbConf.User,
			Password: dbConf.Password,
		})
		db, err := mongo.Connect(context.Background(), client)
		if err != nil {
			log.Fatal(err)
		}

		collection := db.Database(dbConf.Name).Collection("vacancies")
		indexModel := mongo.IndexModel{
			Keys: bson.M{"_id": 1},
		}
		_, err = collection.Indexes().CreateOne(context.Background(), indexModel)
		if err != nil {
			log.Fatal(err)
		}
		return nil, collection, nil
	default:
		log.Fatalln("database drivers don't found")
	}
	return nil, nil, errors.New("can't create new db")
}
