package service

import (
	"context"
	"gitlab.com/GoGerman/go-parser/internal/models"
)

type Parser interface {
	Create(ctx context.Context, lang string) error
	GetById(ctx context.Context, id int) (models.Vacancy, error)
	GetList(ctx context.Context) ([]models.Vacancy, error)
	Delete(ctx context.Context, id int) error
}

type VacancyService struct {
	Data Parser
}

func NewVacancyService(repo Parser) Parser {
	return &VacancyService{Data: repo}
}

func (v *VacancyService) Create(ctx context.Context, lang string) error {
	return v.Data.Create(ctx, lang)
}

func (v *VacancyService) GetById(ctx context.Context, id int) (models.Vacancy, error) {
	return v.Data.GetById(ctx, id)
}

func (v *VacancyService) GetList(ctx context.Context) ([]models.Vacancy, error) {
	return v.Data.GetList(ctx)
}

func (v *VacancyService) Delete(ctx context.Context, id int) error {
	return v.Data.Delete(ctx, id)
}
